﻿using NMeCab;



public class MecabWrapper
{
    private MeCabTagger tagger;

    public MecabWrapper()
    {
        MeCabParam param = new MeCabParam();
        param.DicDir = "Assets/mecabdir/ipadic";
        tagger = MeCabTagger.Create(param);
    }

    public string Parse(string s)
    {
        return tagger.Parse(s);
    }
}

