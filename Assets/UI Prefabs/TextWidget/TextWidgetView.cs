﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;

public class TextWidgetView : MonoBehaviour
{
    [SerializeField]
    List<string> rawPhrases = new List<string>();
    [SerializeField]
    TextMeshProUGUI text;
    [SerializeField]
    List<int> phraseStartIndex = new List<int>();
    [SerializeField]
    int previousSelectedPhrase = -1;
    [SerializeField]
    Color selected;
    [SerializeField]
    Color unselected;
    string colorChangeString = "<color=#";
    StringBuilder aux = new StringBuilder();

    // Use this for initialization
    void Start()
    {

    }

    public void AddPhrase(string phrase)
    {

        rawPhrases.Add(phrase);
        var previousText = text.text;
        phraseStartIndex.Add(previousText.Length);
        aux.Length = 0;
        aux.Append(previousText);
        aux.Append(colorChangeString);
        aux.Append(ColorUtility.ToHtmlStringRGBA(unselected));
        aux.Append('>');
        aux.Append(phrase);
        text.text = aux.ToString();
    }

    public void SelectPhrase(int phrase)
    {
        if (previousSelectedPhrase >= 0)
        {
            var previous = phraseStartIndex[previousSelectedPhrase];
            text.text = ChangeValueInString(text.text, previous + colorChangeString.Length, ColorUtility.ToHtmlStringRGBA(unselected));
        }
        text.text = ChangeValueInString(text.text, phraseStartIndex[phrase] + colorChangeString.Length, ColorUtility.ToHtmlStringRGBA(selected));
        previousSelectedPhrase = phrase;
    }

    private string ChangeValueInString(string text, int startIndex, string stringToInsert)
    {
        aux.Length = 0;
        aux.Append(text);
        for (int i = 0; i < stringToInsert.Length; i++)
        {
            aux[i + startIndex] = stringToInsert[i];

        }
        return aux.ToString();
    }

    // Update is called once per frame
    void Update()
    {

    }

    [ContextMenu("Add phrase")]
    public void TestAddPhrase()
    {
        AddPhrase("blablabla");
    }

    [ContextMenu("Select last")]
    public void SelectLast()
    {
        SelectPhrase(rawPhrases.Count-1);
    }

    [ContextMenu("advance select")]
    public void AdvanceSelect()
    {
        int newSele = previousSelectedPhrase;
        newSele++;
        if (newSele >= rawPhrases.Count) {
            newSele = 0;
        }
        SelectPhrase(newSele);
    }
}
