﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TextWidgetViewB : MonoBehaviour
{
    //[SerializeField]
    //List<string> rawPhrases = new List<string>();
    [SerializeField]
    Text backlog;
    [SerializeField]
    Text newText;

    public ScrollRect scrollRect;

    [SerializeField]
    string currentPhrase;

    StringBuilder aux = new StringBuilder();

    // Use this for initialization
    void Start()
    {

    }

    public void ShowPhrase(string phrase)
    {
        backlog.text += currentPhrase;
        currentPhrase = phrase;
        newText.text = phrase;
        Canvas.ForceUpdateCanvases();
        scrollRect.verticalNormalizedPosition = 0;
    }


    // Update is called once per frame
    void Update()
    {

    }

    [ContextMenu("Show phrase")]
    public void TestShowPhrase()
    {
        ShowPhrase("blablabla");
    }

    [ContextMenu("Select last")]
    public void SelectLast()
    {
       
    }

    [ContextMenu("advance select")]
    public void AdvanceSelect()
    {

    }
}
