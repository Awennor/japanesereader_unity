﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[SerializeField]
public class ButtonScrollList : MonoBehaviour
{

    [SerializeField]
    List<string> buttonTexts = new List<string>();
    [SerializeField]
    string upperText;
    [SerializeField]
    List<Button> buttons = new List<Button>();
    [SerializeField]
    Transform buttonParent;
    public GameObject uiHolder;

    internal void Show(bool v)
    {
        uiHolder.SetActive(v);
        if (v)
        {
            UpdateButtons();
        }
    }

    [SerializeField]
    Text upperTextUI;

    public event Action<int> OnButtonPress;

    public List<string> ButtonTexts
    {
        get
        {
            return buttonTexts;
        }

        set
        {
            buttonTexts = value;
        }
    }

    public string UpperText
    {
        get
        {
            return upperText;
        }

        set
        {
            upperText = value;
        }
    }

    // Use this for initialization
    void Start()
    {
        foreach (var b in buttons)
        {
            SetupButton(b);
        }

    }

    [ContextMenu("Update Buttons")]
    public void UpdateButtons()
    {
        upperTextUI.text = upperText;
        while (buttons.Count < buttonTexts.Count)
        {
            Button button = Instantiate(buttons[0], buttonParent);
            buttons.Add(button);
            SetupButton(button);
        }
        for (int i = 0; i < buttons.Count; i++)
        {
            buttons[i].gameObject.SetActive(i < buttonTexts.Count);
            if (i < buttonTexts.Count)
            {
                buttons[i].GetComponentInChildren<Text>().text = buttonTexts[i];
            }
        }
    }

    private void SetupButton(Button b)
    {
        b.onClick.AddListener(() =>
       {
           OnButtonPress(buttons.IndexOf(b));
       });
    }

    // Update is called once per frame
    void Update()
    {

    }
}
