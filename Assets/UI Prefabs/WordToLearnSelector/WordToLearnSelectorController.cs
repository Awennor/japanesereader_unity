﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordToLearnSelectorController : MonoBehaviour
{
    List<string> words = new List<string>();
    List<string> meanings = new List<string>();

    public event Action<string> OnWordSelected;

    [SerializeField]
    ButtonScrollList buttonScrollList;

    // Use this for initialization
    void Start()
    {
        buttonScrollList.OnButtonPress += ButtonScrollList_OnButtonPress;
    }

    private void ButtonScrollList_OnButtonPress(int obj)
    {
        
        OnWordSelected(words[obj]);
    }

    public void Show()
    {
        
        buttonScrollList.ButtonTexts.Clear();
        for (int i = 0; i < words.Count; i++)
        {
            buttonScrollList.ButtonTexts.Add(words[i] + "-" + meanings[i]);
        }
        buttonScrollList.Show(true);
    }

    // Update is called once per frame
    void Update()
    {

    }

    internal void Add(string w, string trans)
    {
        words.Add(w);
        meanings.Add(trans);
    }

    internal void Clear()
    {
        words.Clear();
        meanings.Clear();
    }
}
