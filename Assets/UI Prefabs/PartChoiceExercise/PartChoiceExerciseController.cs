﻿using JapaneseTextReader.exercise;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartChoiceExerciseController : MonoBehaviour
{

    PartChoiceExercise Exercise { get; set; }
    public PartChoiceExerciseView view;
    List<int> partsDone = new List<int>();
    Dictionary<int, string> partsChosen = new Dictionary<int, string>();
    int currentPart;
    private bool exerciseCorrect;
    public event Action<bool, PartChoiceExercise> OnExerciseAnswered;
    public GameObject viewObject;

    // Use this for initialization
    void Start()
    {
        view.OnPartChosen += View_OnButtonPress;
        view.OnEndResult += View_OnEndResult;


    }

    [ContextMenu("TestExercise")]
    public void TestExercise() {
        if (Exercise == null) GenerateTestExercise();
        ShowExercise();
    }

    private void ShowExercise()
    {
        ShowView(true);
        //view.Prompt = Exercise.Prompt;
        //view.PromptExtra = Exercise.ContextPhrase;
        view.PromptExtra = "";
        view.Prompt = Exercise.ContextPhrase.Replace(Exercise.Prompt,"<b><color=black>"+ Exercise.Prompt+ "</color></b>");
        //De

        view.NumberOfPortions = Exercise.AnswerParts.Count;
        currentPart = 0;
        CurrentPartValidity();
        int part = currentPart;
        view.SelectedPortion = part;

        partsChosen.Clear();
        foreach (var i in Exercise.ShownParts)
        {
            partsChosen[i] = Exercise.AnswerParts[i];
        }
        
        SetPartsChosen();
        UpdateDummies(part);
        
        view.Show();
    }

    private void CurrentPartValidity()
    {
        while (Exercise.ShownParts.Contains(currentPart)) {
            currentPart++;
        }
    }

    private void View_OnEndResult()
    {
        OnExerciseAnswered(exerciseCorrect, Exercise);
    }

    private void UpdateDummies(int part)
    {
        view.ButtonTexts.Clear();
        foreach (var item in Exercise.Dummies[part])
        {
            view.ButtonTexts.Add(item);
        }
        view.ButtonTexts.Add(Exercise.AnswerParts[part]);
        view.ButtonTexts.Shuffle();
    }

    internal void ShowExercise(PartChoiceExercise ex)
    {
        Exercise = ex;
        ShowExercise();
    }

    private void SetPartsChosen()
    {
        for (int i = 0; i < Exercise.AnswerParts.Count; i++)
        {
            if (partsChosen.ContainsKey(i))
            {
                view.SetPartText(i, partsChosen[i]);
            }
            else{
                view.NoTextOnPart(i);
            }
        }
    }

    private void View_OnButtonPress(int arg1, string buttonText)
    {
        partsChosen[currentPart] = buttonText;
        currentPart++;
        CurrentPartValidity();
        SetPartsChosen();
        if (currentPart >= Exercise.AnswerParts.Count)
        {
            //currentPart = -1;
            bool correct = true;
            for (int i = 0; i < Exercise.AnswerParts.Count; i++)
            {
                if (Exercise.AnswerParts[i] != partsChosen[i])
                {
                    correct = false;
                    break;
                }
            }
            exerciseCorrect = correct;
            view.ShowResult(Exercise.Answer, Exercise.Prompt, correct);
            view.Show();
            return;
        }

        view.SelectedPortion = currentPart;
        UpdateDummies(currentPart);
        view.Show();
    }

    internal void ShowExercise(object p)
    {
        throw new NotImplementedException();
    }

    internal void ShowView(bool show)
    {
        viewObject.SetActive(show);
    }

    private void GenerateTestExercise()
    {
        var ex = new PartChoiceExercise("Car", "blabluble");
        Exercise = ex;
        ex.AnswerParts.AddRange(new string[] { "bla", "blu", "ble" });

        {
            List<string> list = new List<string>();
            list.AddRange(new string[] {
            "blo", "rex" });
            ex.Dummies.Add(list);
        };
        {
            List<string> list = new List<string>();
            list.AddRange(new string[] {
            "king", "melex", "vraptor" });
            ex.Dummies.Add(list);
        };
        {
            List<string> list = new List<string>();
            list.AddRange(new string[] {
            "Drrr", "melex", "vermillion" });
            ex.Dummies.Add(list);
        };
    }

    // Update is called once per frame
    void Update()
    {

    }
}
