﻿using pidroh.JapaneseTextReader;
using pidroh.JapaneseTextReader.persistence;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JapaneseAppSetup : MonoBehaviour
{

    private const string SerializeKey = "learnerdata0";

    public TextAsset dictionaryText;

    // Use this for initialization
    void Awake()
    {
        if (GenericDataHolder.Instance.GetObject<JapaneseApp>() == null)
        {
            var ja = new JapaneseApp();
            var learnerData = Persistence.Load<LearnerData>(SerializeKey);
            if (learnerData == null) learnerData = new LearnerData();
            //learnerData.learnerWordData.config_WrongExerciseInterval = 6;
            //learnerData.learnerWordData.config_FirstExerciseInterval = 8;
            //learnerData.learnerWordData.config_MinCorrectInterval= 20;

            ja.Setup(new MecabWrapper().Parse, dictionaryText.text, learnerData);
            GenericDataHolder.Instance.Register(ja);
        }
        
    }

    public void ClearProgress()
    {
        PlayerPrefs.DeleteAll();
        GenericDataHolder.Instance.GetObject<JapaneseApp>().ChangeLearnerData(new LearnerData());
    }

    public void ClearWordData()
    {
        
        GenericDataHolder.Instance.GetObject<JapaneseApp>().LearnerData.learnerWordData.ClearAllData();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
