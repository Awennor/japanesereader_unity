﻿using pidroh.JapaneseTextReader;
using pidroh.JapaneseTextReader.analytics;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordListController : MonoBehaviour {

    public PartChoiceExerciseController exercisePrefab;
    private PartChoiceExerciseController partChoiceExerciseController;
    private WordIntroductionController wordIntroductionController;
    public WordIntroductionController wordIntroPrefab;
    public RectTransform parent;

    static string[] words = new string[] {
            "Urumpt",
            "Deolom",
            "Jejurm",
            "Rutboo",
            "Abogad",
            "Torser",
            "Kricul",
            "Psycla",
            "Luritt",
            "Movant"
        };
    static string[] meanings = new string[] {
            "Water",
            "Robot",
            "King",
            "Duck",
            "Giant",
            "Bat",
            "Bench",
            "Dragon",
            "Planet",
            "Dog",
        };
    private WordListLearnerNavigator wordListLearnerNavigator;

    // Use this for initialization
    void Start () {
        
        partChoiceExerciseController = Instantiate(exercisePrefab, parent);
        partChoiceExerciseController.ShowView(false);
        wordIntroductionController = Instantiate(wordIntroPrefab, parent);
        wordIntroductionController.HideView();
        wordIntroductionController.View.OnAdvancePress += View_OnAdvancePress;
        partChoiceExerciseController.OnExerciseAnswered += PartChoiceExerciseController_OnExerciseAnswered;

        LearnerWordDataAccessor learningProfile = new LearnerWordDataAccessor();

        wordListLearnerNavigator = new WordListLearnerNavigator();
        wordListLearnerNavigator.Initialize(words, meanings);
        wordListLearnerNavigator.OnIntroduceWordIntent += WordListLearnerNavigator_OnIntroduceWordIntent;
        wordListLearnerNavigator.OnExerciseWordIntent += WordListLearnerNavigator_OnExerciseWordIntent;
        wordListLearnerNavigator.Start();
    }

    private void WordListLearnerNavigator_OnExerciseWordIntent(JapaneseTextReader.exercise.PartChoiceExercise obj)
    {
        partChoiceExerciseController.ShowExercise(obj);
    }

    private void PartChoiceExerciseController_OnExerciseAnswered(bool arg1, JapaneseTextReader.exercise.PartChoiceExercise arg2)
    {
        wordListLearnerNavigator.ExerciseReport(arg1);
        partChoiceExerciseController.ShowView(false);
        wordListLearnerNavigator.Advance();
    }

    private void View_OnAdvancePress()
    {
        wordIntroductionController.HideView();
        wordListLearnerNavigator.Advance();
    }

    private void WordListLearnerNavigator_OnIntroduceWordIntent(string arg1, string arg2)
    {
        wordIntroductionController.ShowWord(arg1, arg2);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
