﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using UnityEngine;

public class GenericDataHolder{

    private static GenericDataHolder instance;

    //Dictionary<Type, object> holder = new Dictionary<, object>();
    ServiceContainer serviceContainer = new ServiceContainer();

    public static GenericDataHolder Instance
    {
        get
        {
            if (instance == null) instance = new GenericDataHolder();
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    public void Register(object o) {
        
        Type type = o.GetType();
        serviceContainer.RemoveService(type);
        serviceContainer.AddService(type, o);
    }

    public T GetObject<T>() {
        object v = serviceContainer.GetService(typeof(T));
        if (v == null) return default(T);
        return (T)v;
    }



}
