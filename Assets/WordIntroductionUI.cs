﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class WordIntroductionUI : MonoBehaviour {

    [SerializeField]
    List<string> words = new List<string>();
    [SerializeField]
    List<string> meanings = new List<string>();
    public Text wordText;
    public Text meaningText;
    StringBuilder stringBuilder = new StringBuilder();
    public event Action OnAdvancePress;

	// Use this for initialization
	void Start () {
		
	}

    public void AdvancePress_External() {
        if(OnAdvancePress!= null)
            OnAdvancePress();
    }

    internal void Add(string newWordToLearn, string meaning)
    {
        words.Add(newWordToLearn);
        meanings.Add(meaning);
    }

    internal void Clear()
    {
        words.Clear();
        meanings.Clear();
    }

    [ContextMenu("Show")]
    public void Show() {
        stringBuilder.Length = 0;
        for (int i = 0; i < words.Count; i++)
        {
            stringBuilder.Append(words[i]);

            if (i != words.Count - 1)
                stringBuilder.AppendLine();
        }
        wordText.text = stringBuilder.ToString();
        stringBuilder.Length = 0;
        for (int i = 0; i < words.Count; i++)
        {
            stringBuilder.Append(meanings[i]);
            if (i != words.Count - 1)
                stringBuilder.AppendLine();
        }
        meaningText.text = stringBuilder.ToString();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
