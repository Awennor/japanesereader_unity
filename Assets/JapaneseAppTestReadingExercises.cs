﻿
using pidroh.JapaneseTextReader;
using pidroh.JapaneseTextReader.analytics;
using pidroh.JapaneseTextReader.persistence;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JapaneseAppTestReadingExercises : MonoBehaviour
{
    private const string SerializeKey = "learnerdata0";
    public TextAsset dictionaryText;
    public TextAsset storyText;
    public PartChoiceExerciseController partChoiceExerciseController;
    //private PartChoiceExerciseController partChoiceExerciseController;
    //private WordIntroductionController wordIntroductionController;
    //public WordIntroductionController wordIntroPrefab;

    public WordToLearnSelectorController wordToLearnSelector;

    public TextWidgetViewB textWidget;
    private JapaneseReaderSuper jrs;
    int currentPhrase;
    private LearnerData learnerData;
    private JapaneseApp japp;

    // Use this for initialization
    void Start()
    {
        japp = GenericDataHolder.Instance.GetObject<JapaneseApp>();
        if (japp == null)
        {
            string storyText = this.storyText.text;

            MecabWrapper mecabWrapper = new MecabWrapper();
            learnerData = Persistence.Load<LearnerData>(SerializeKey);
            if (learnerData == null) learnerData = new LearnerData();
            jrs = new JapaneseReaderSuper(storyText, "title", dictionaryText.text, mecabWrapper.Parse, learnerData);
        }
        else
        {
            learnerData = japp.LearnerData;
            jrs = japp.Jsr;
        }

        
        wordToLearnSelector.OnWordSelected += WordToLearnSelector_OnWordSelected;
        partChoiceExerciseController.OnExerciseAnswered += PartChoiceExerciseController_OnExerciseAnswered;
        int seg = jrs.GetCurrentSegment();
        for (int i = 0; i < seg; i++)
        {
            textWidget.ShowPhrase(jrs.GetPhrase(i));
        }
        ShowPhrase();
    }

    private void PartChoiceExerciseController_OnExerciseAnswered(bool arg1, JapaneseTextReader.exercise.PartChoiceExercise arg2)
    {
        partChoiceExerciseController.ShowView(false);
        //jrs.ExerciseResult(arg1);
        japp.ReadingExerciseManager.ReportOnExercise(arg1);
        Advance();
    }

    private void WordToLearnSelector_OnWordSelected(string obj)
    {
        jrs.LearnWordFromCurrentSegment(obj);
        AddWordPress_External();
    }

    private void ShowPhrase()
    {
        var phrase = jrs.GetCurrentPhrase();
        while (phrase.Trim().Length == 0) {
            textWidget.ShowPhrase(phrase);
            jrs.AdvanceSegment();
            phrase = jrs.GetCurrentPhrase();
        }
        textWidget.ShowPhrase(phrase);
    }

    public void AddWordPress_External()
    {
        var words = jrs.GetWords(jrs.GetCurrentSegment());
        wordToLearnSelector.Clear();
        foreach (var w in words)
        {
            if (!jrs.KnownWord(w))
                wordToLearnSelector.Add(w, jrs.GetTranslation(w));
        }
        wordToLearnSelector.Show();
    }

    public void Advance_External()
    {
        Advance();
    }

    private void Advance()
    {

        //jrs.Save(Persistence.SaveMethod, "superunity_");
        var re = japp.GetNewReadingExercise();
        if (re != null) {
            partChoiceExerciseController.ShowExercise(re);
            return;
        }
        
        if (jrs.ShouldReview())
        {
            partChoiceExerciseController.ShowExercise(jrs.ReviewExercise());
        }
        else
        {
            if (jrs.CanAdvanceSegment())
            {
                jrs.AdvanceSegment();
                ShowPhrase();
            }
            
        }
        Persistence.Save(SerializeKey, learnerData);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
