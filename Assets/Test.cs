﻿using JapaneseTextReader;
using NMeCab;
using pidroh.JapaneseTextReader.analytics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Test : MonoBehaviour
{

    public TextAsset dictionaryText;
    public TextAsset storyText;
    public PartChoiceExerciseController exercisePrefab;
    private PartChoiceExerciseController partChoiceExerciseController;
    private WordIntroductionController wordIntroductionController;
    public WordIntroductionController wordIntroPrefab;


    // Use this for initialization
    void Start()
    {
        partChoiceExerciseController = Instantiate(exercisePrefab, this.transform);
        wordIntroductionController = Instantiate(wordIntroPrefab, this.transform);
        wordIntroductionController.HideView();

        

        LearnerWordDataAccessor learningProfile = new LearnerWordDataAccessor();

        string storyText = this.storyText.text;

        MecabWrapper mecabWrapper = new MecabWrapper();
        Debug.Log(mecabWrapper.Parse(storyText));
        var jptr = new JapaneseReader(dictionaryText.text, storyText, mecabWrapper.Parse);

        int currentSeg = 0;
        int currentWord = 0;

        var adv = jptr.Advancer;

        currentSeg = adv.CurrentSegment;
        currentWord = adv.CurrentWord;


        Action exercise = () =>
        {
            currentSeg = adv.CurrentSegment;
            currentWord = adv.CurrentWord;
            var ex = jptr.GetExercise(currentSeg, currentWord);
            partChoiceExerciseController.ShowExercise(ex);
        };

        wordIntroductionController.View.OnAdvancePress += () =>
        {
            wordIntroductionController.HideView();
            exercise();
        };

        Action word = () =>
        {
            
            var newWordToLearn = jptr.GetWord();

            while (learningProfile.IsKnownWord(newWordToLearn))
            {
                Debug.Log("Known");
                adv.Advance();
                newWordToLearn = jptr.GetWord();
            }
            Debug.Log("Unknown");
            currentSeg = adv.CurrentSegment;
            currentWord = adv.CurrentWord;
            //learningProfile.Learn
            var meaning = jptr.GetTranslation(currentSeg, currentWord);
            wordIntroductionController.ShowWord(newWordToLearn, meaning);


        };

        word();
        partChoiceExerciseController.OnExerciseAnswered += (b, ex) =>
        {
            learningProfile.ExercisedWord(ex.Answer, b);
            learningProfile.Interacted();
            partChoiceExerciseController.ShowView(false);
            adv.Advance();

            //if (true) { 
            if (learningProfile.ShouldReview())
            {
                var wordToReview = learningProfile.GetWordToReview();
                if (wordToReview != null)
                {
                    //partChoiceExerciseController.ShowExercise(jptr.GetReviewExercise(wordToReview, learningProfile));
                    return;
                }


            }
            word();
            //exercise();
        };

        //string word = jptr.GetWord(currentSeg, currentWord);
        //Debug.Log(word);


    }



    // Update is called once per frame
    void Update()
    {

    }
}
