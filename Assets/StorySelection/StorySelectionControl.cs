﻿using JapaneseTextReader;
using pidroh.JapaneseTextReader;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StorySelectionControl : MonoBehaviour {

    public TextAsset[] texts;
    public ButtonScrollList buttonList;
    public string scene_AfterChoosingStory;

	// Use this for initialization
	void Start () {
        buttonList.ButtonTexts.Clear();
        var ja = GenericDataHolder.Instance.GetObject<JapaneseApp>();
        foreach (var t in texts)
        {
            string item = string.Format("{0} - {1}%",t.name, ja.GetStoryProgress(t.name)*100);
            buttonList.ButtonTexts.Add(item);
        }
        buttonList.Show(true);
        buttonList.OnButtonPress += ButtonList_OnButtonPress;

        
	}

    private void ButtonList_OnButtonPress(int obj)
    {
        var t = texts[obj];
        var ja = GenericDataHolder.Instance.GetObject<JapaneseApp>();
        if (!ja.HasStory(t.name)) {
            //ja.AddStory(t.name, t.text);
            TextMetaData td = Persistence.DeserializeBytes<TextMetaData>(t.bytes);
            ja.AddStory(td);
        }
        
        ja.ChooseStory(t.name);
        SceneManager.LoadScene(scene_AfterChoosingStory);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
