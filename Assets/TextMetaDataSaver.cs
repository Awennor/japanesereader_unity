﻿using pidroh.JapaneseTextReader;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class TextMetaDataSaver : MonoBehaviour
{

    
    public TextAsset[] textAssets;
    public TextAsset dictionaryText;
    public string dir;
    public Object[] textAsset;
    public TextAsset[] deserializeTest;

    // Use this for initialization
    void Start()
    {

        
    }
    [ContextMenu("Create files")]
    public void CreateFiles()
    {
        
        var fac = new TextFactory();
        fac.Setup(new MecabWrapper().Parse, dictionaryText.text);
        foreach (var t in textAssets)
        {
            var tmd = fac.CreateTextMeta(t.name, t.text);
            string path = dir + t.name + ".bytes";
            FileStream fs = new FileStream(path, FileMode.Create);

            // Construct a BinaryFormatter and use it to serialize the data to the stream.
            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(fs, tmd);
            }
            catch (SerializationException e)
            {
                throw;
            }
            finally
            {
                fs.Close();
            }
            Debug.Log("Saved file at "+path);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
