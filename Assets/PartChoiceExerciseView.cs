﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class PartChoiceExerciseView : MonoBehaviour
{
    [SerializeField]
    private string promptExtra;
    [SerializeField]
    private string prompt;
    [SerializeField]
    private List<string> buttonTexts = new List<string>();

    [SerializeField]
    private int selectedPortion;
    [SerializeField]
    private int numberOfPortions;

    public Text promptText;
    public Text promptTextExtra;
    public Text partsText;
    public Text resultText;
    public GameObject resultGO;
    public Button[] buttons;
    public event Action<int, string> OnPartChosen;
    public event Action OnEndResult;
    Dictionary<int, string> partsChosen = new Dictionary<int, string>();

    const string emptyPortionText = "___";
    const string selectedPortionText = "<color=red>_______</color>";
    const string spacing = "    ";


    StringBuilder sb = new StringBuilder();

    [ContextMenu("Show")]
    internal void Show()
    {
        BuildPartsText();
        promptTextExtra.text = promptExtra;
        promptText.text = prompt;
        for (int i = 0; i < buttons.Length; i++)
        {
            bool active = i < buttonTexts.Count;
            buttons[i].gameObject.SetActive(active);
            if (active)
            {
                buttons[i].transform.GetComponentInChildren<Text>().text = buttonTexts[i];
            }
        }
    }

    public void OnEndResultButtonPress_External()
    {
        if(OnEndResult != null)
            OnEndResult();
        resultGO.SetActive(false);
    }

    private void BuildPartsText()
    {
        sb.Length = 0;
        bool previousEmpty = false;
        for (int i = 0; i < numberOfPortions; i++)
        {

            if (partsChosen.ContainsKey(i))
            {
                previousEmpty = false;
                if (i > 0)
                {
                    sb.Append(spacing);
                }
                sb.Append(partsChosen[i]);
                
            }
            else
            {
                if (i > 0)
                {
                    if (previousEmpty || true)
                    {
                        sb.Append(spacing);
                    }
                }
                if (i == selectedPortion)
                {
                    sb.Append(selectedPortionText);
                }
                else
                {
                    sb.Append(emptyPortionText);

                }

                previousEmpty = true;
            }
        }
        partsText.text = sb.ToString();
    }

    internal void NoTextOnPart(int i)
    {
        partsChosen.Remove(i);
    }

    internal void ShowResult(string answer, string prompt, bool correct)
    {
        sb.Length = 0;
        string result = "You got it right!";
        if (!correct) result = "You did't get it correct this time...";
        sb.AppendFormat("\"{1}\"\n<size={3}></size><b>\n{0}</b>\n\n{2}", answer, prompt, result, resultText.fontSize * 0.6f);
        resultText.text = sb.ToString();
        resultGO.SetActive(true);
    }

    public void Awake()
    {
        resultGO.SetActive(false);
        for (int i = 0; i < buttons.Length; i++)
        {
            int buttonId = i;
            buttons[i].onClick.AddListener(() =>
            {
                string buttonText = ButtonTexts[buttonId];
                OnPartChosen(buttonId, buttonText);
            });
        }
    }

    internal void SetPartText(int i, string v)
    {
        partsChosen[i] = v;
    }

    public string Prompt
    {
        get
        {
            return prompt;
        }

        set
        {
            prompt = value;
        }
    }

    public List<string> ButtonTexts
    {
        get
        {
            return buttonTexts;
        }

        set
        {
            buttonTexts = value;
        }
    }




    public int SelectedPortion
    {
        get
        {
            return selectedPortion;
        }

        set
        {
            selectedPortion = value;
        }
    }

    public int NumberOfPortions
    {
        get
        {
            return numberOfPortions;
        }

        set
        {
            numberOfPortions = value;
        }
    }

    public string PromptExtra
    {
        get
        {
            return promptExtra;
        }

        set
        {
            promptExtra = value;
        }
    }


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
