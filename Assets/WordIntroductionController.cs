﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordIntroductionController : MonoBehaviour {

    [SerializeField]
    WordIntroductionUI view;

    public WordIntroductionUI View
    {
        get
        {
            return view;
        }

        set
        {
            view = value;
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    internal void ShowWord(string newWordToLearn, string meaning)
    {
        view.Clear();
        view.Add(newWordToLearn, meaning);
        view.gameObject.SetActive(true);
        view.Show();
    }

    internal void HideView()
    {
        view.gameObject.SetActive(false);
    }
}
