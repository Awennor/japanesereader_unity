﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using pidroh.JapaneseTextReader.persistence;
using UnityEngine;

public class Persistence 
{

    static BinaryFormatter binaryFormatter = new BinaryFormatter();



    internal static void Save(string serializeKey, object obj)
    {
        PlayerPrefs.SetString(serializeKey, SerializeToString(obj));
        PlayerPrefs.Save();
    }

    internal static T Load<T>(string serializeKey)
    {
        if (PlayerPrefs.HasKey(serializeKey))
        {
            return DeserializeFromString<T>(PlayerPrefs.GetString(serializeKey));
        }
        else
        {
            return default(T);
        }
    }

    private static TData DeserializeFromString<TData>(string settings)
    {
        byte[] b = Convert.FromBase64String(settings);
        return DeserializeBytes<TData>(b);
    }

    public static TData DeserializeBytes<TData>(byte[] b)
    {
        using (var stream = new MemoryStream(b))
        {
            var formatter = new BinaryFormatter();
            stream.Seek(0, SeekOrigin.Begin);
            return (TData)formatter.Deserialize(stream);
        }
    }

    private static string SerializeToString(object settings)
    {
        using (var stream = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            formatter.Serialize(stream, settings);
            stream.Flush();
            stream.Position = 0;
            return Convert.ToBase64String(stream.ToArray());
        }
    }
}
