﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using UnityEngine;

class Persistence
{
    static BinaryFormatter formatter = new BinaryFormatter();

    internal static void SaveMethod(string arg1, object arg2)
    {
        var s = SerializeToString(arg2);
        PlayerPrefs.SetString(arg1, s);
    }

    internal static T LoadMethod<T>(string arg1)
    {
        if (PlayerPrefs.HasKey(arg1))
        {
            return default(T);
        }
        else
        {
            return DeserializeFromString<T>(PlayerPrefs.GetString(arg1));
        }
    }

    private static string SerializeToString<TData>(TData settings)
    {
        using (var stream = new MemoryStream())
        {
            formatter.Serialize(stream, settings);
            stream.Flush();
            stream.Position = 0;
            return Convert.ToBase64String(stream.ToArray());
        }
    }

    private static TData DeserializeFromString<TData>(string settings)
    {
        byte[] b = Convert.FromBase64String(settings);
        using (var stream = new MemoryStream(b))
        {
            stream.Seek(0, SeekOrigin.Begin);
            return (TData)formatter.Deserialize(stream);
        }
    }
}

